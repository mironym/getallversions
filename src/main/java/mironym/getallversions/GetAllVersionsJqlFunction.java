package mironym.getallversions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;

/**
 * This is the main and only class implementing the logic of the Get All
 * Versions plugin, see https://bitbucket.org/mironym/getallversions/wiki/Home
 * for more information.
 * 
 * @author mironym
 *
 */
public class GetAllVersionsJqlFunction extends AbstractJqlFunction {
	/**
	 * Implements the logic, returns the versions that match the beginning of
	 * the string specified in the single argument.
	 * 
	 * @see com.atlassian.jira.plugin.jql.function.JqlFunction#getValues(com.atlassian.jira.jql.query.QueryCreationContext,
	 *      com.atlassian.query.operand.FunctionOperand,
	 *      com.atlassian.query.clause.TerminalClause)
	 */
	public List<QueryLiteral> getValues(QueryCreationContext queryCreationContext, FunctionOperand operand,
			TerminalClause terminalClause) {
		final List<String> arguments = operand.getArgs();

		if (arguments.isEmpty()) {
			return Collections.emptyList();
		}

		return getNameLiteralsForVersions(getMatchingVersions(arguments.get(0)), operand);
	}

	/**
	 * @return 1
	 */
	public int getMinimumNumberOfExpectedArguments() {
		return 1;
	}

	/**
	 * Returns (list) of Jira VERSIONS.
	 * 
	 * @return JiraDataTypes.VERSION
	 */
	public JiraDataType getDataType() {
		return JiraDataTypes.VERSION;
	}

	/**
	 * It just validates if there is at least one argument.
	 * 
	 */
	@Override
	public MessageSet validate(ApplicationUser user, FunctionOperand operand, TerminalClause clause) {
		MessageSet messages = new MessageSetImpl();
		final List<String> arguments = operand.getArgs();

		// Make sure we have the correct number of arguments.
		if (arguments.isEmpty()) {
			messages.addErrorMessage("The function requires 1 argument - the begin of the versions to be matched.");
			return messages;
		}

		// TODO some more validations?
		return messages;
	}

	/**
	 * Aux method - it retrieves all versions in the system and returns only
	 * those that have name starting with the argument.
	 * 
	 * @param versionBegin
	 * @return
	 */
	private Collection<Version> getMatchingVersions(String versionBegin) {
		Collection<Version> allVersions = ComponentAccessor.getVersionManager().getAllVersions();

		Collection<Version> matchingVersions = new ArrayList<Version>();

		for (Iterator<Version> iterator = allVersions.iterator(); iterator.hasNext();) {
			Version version = (Version) iterator.next();

			if (version.getName().startsWith(versionBegin)) {
				matchingVersions.add(version);
			}
		}

		return matchingVersions;
	}

	/**
	 * Aux method - it transforms the collection of the versions to the list of
	 * version names wrapped as the QueryLiterals.
	 * 
	 * @param versions
	 * @param operand
	 * @return
	 */
	private List<QueryLiteral> getNameLiteralsForVersions(Collection<Version> versions, FunctionOperand operand) {
		final List<QueryLiteral> literals = new LinkedList<QueryLiteral>();

		for (Iterator<Version> iterator = versions.iterator(); iterator.hasNext();) {
			Version version = (Version) iterator.next();

			literals.add(new QueryLiteral(operand, version.getName()));

		}

		return literals;
	}
}